import { CardInterface } from "./Collection";
import "./CardForm.scss";
import { useValidation } from "../hooks/useValidation.tsx";
import { cardFormValidation } from "../hooks/validations/cardFormValidation.tsx";
import Input from "./Input.tsx";

interface CardFormProps {
    visible: boolean;
    addCard: (addCard: CardInterface) => void;
    setFormVisible: React.Dispatch<React.SetStateAction<boolean>>;
}

const CardForm: React.FC<CardFormProps> = ({ visible, addCard, setFormVisible }) => {

    const { values, errors, handleSubmit, handleChange } = useValidation(createNewCard, cardFormValidation);

    function createNewCard() {
        const card: CardInterface = {
            name: values.name,
            description: values.description
        };
        addCard(card);
        setFormVisible(false);
    }

    return (
        visible ?
        <div className="card-form">
            <Input
                name="name"
                value={values.name}
                error={errors.name}
                handleChange={handleChange}
            />
            <Input
                name="description"
                value={values.description}
                error={errors.description}
                handleChange={handleChange}
            />
            <button onClick={handleSubmit}> Submit </button>
        </div>
        : null
    );
}

export default CardForm;
