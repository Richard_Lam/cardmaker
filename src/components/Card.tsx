import "./Card.scss";

interface CardProps {
    name: string;
    description: string;
}

const Card: React.FC<CardProps> = ({ name, description }) => {
    return (
        <div className="card">
            <div className="card-header"> {name} </div>
            <div className="profile"> Image URL </div>
            <div className="body"> {description} </div>
        </div>
    );
}

export default Card;
