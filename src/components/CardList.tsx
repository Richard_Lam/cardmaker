import "./CardList.scss";
import Card from "./Card.tsx";
import { CardInterface } from "./Collection";

interface CardListProps {
    cards: CardInterface[]
}

const CardList: React.FC<CardListProps> = ({ cards }) => {

    return (
        <div className="card-list">
            {cards.map((card) => (
                <Card name={card.name} description={card.description} />
            ))}
        </div>
    );
}

export default CardList;
