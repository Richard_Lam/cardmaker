interface InputProps {
    name: string;
    value: string;
    error: string;
    handleChange: (event: any) => void
}

const Input: React.FC<InputProps> = ({ name, value, error, handleChange }) => {
    return (
        <div>
            <input value={value} onChange={handleChange} name={name} />
            {error ?
                <div>
                    {error}
                </div>
            : null}
        </div>
    );
}

export default Input;
