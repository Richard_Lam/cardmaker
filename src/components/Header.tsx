import "./Header.scss";

interface HeaderProps {
    setFormVisible: React.Dispatch<React.SetStateAction<boolean>>;
}

const Header: React.FC<HeaderProps> = ({ setFormVisible }) => {

    return (
        <div className="header">
            <button onClick={() => setFormVisible(true)}> New Card </button>
        </div>
    );
}

export default Header;
