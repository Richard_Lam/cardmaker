import React, { useState } from 'react';
import Header from "./Header.tsx";
import "./Collection.scss";
import CardList from "./CardList.tsx";
import CardForm from "./CardForm.tsx";

export interface CardInterface {
    name: string;
    description: string;
}

const Collection = () => {

    const [cards, setCards] = useState<CardInterface[]>([{name: "Richard Lam", description: "Software Engineer dude, likes running and working out"}]);
    const [formVisible, setFormVisible] = useState<boolean>(false);

    const addCard = (card: CardInterface) => {
        setCards([...cards, card]);
    }

    return (
        <div className="collection">
            <Header setFormVisible={setFormVisible}/>
            <CardList cards={cards} />
            <CardForm visible={formVisible} addCard={addCard} setFormVisible={setFormVisible} />
        </div>
    );
}

export default Collection;
