import Collection from "./components/Collection.tsx";

function App() {
  return (
    <div style={{ height: '100vh' }}>
      <Collection />
    </div>
  );
}

export default App;
