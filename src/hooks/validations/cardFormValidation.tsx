import { CardInterface } from "../../components/Collection";

export const cardFormValidation = (card: CardInterface) => {
    const errors: any = {};
    if (!card.name) {
        errors.name = "Name is required!";
    }
    if (!card.description) {
        errors.description = "Description is required!";
    }
    return errors;
}
