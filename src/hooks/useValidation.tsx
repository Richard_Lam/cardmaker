// https://upmostly.com/tutorials/form-validation-using-custom-react-hooks#:~:text=Setting%20Up%20Form%20Validation%20Using%20React%20Hooks&text=We%20need%20to%20do%20several,submitting%20if%20any%20errors%20exist
// This "trick" learned here
import { useEffect, useState } from "react";

export const useValidation = (onSuccess: any, validate: any) => {
    const [values, setValues] = useState<any>({});
    const [errors, setErrors] = useState<any>({});
    const [isSubmitting, setIsSubmitting] = useState(false);

    const handleSubmit = (event: any) => {
        if (event) event.preventDefault();
        setErrors(validate(values));
        setIsSubmitting(true);
    }

    const handleChange = (event: any) => {
        event.persist();
        setValues(values => ({ ...values, [event.target.name]: event.target.value }));
    };

    useEffect(() => {
        if (Object.keys(errors).length === 0 && isSubmitting) {
          onSuccess();
          setIsSubmitting(false);
        }
      }, [errors, isSubmitting, onSuccess]);

    return {
        values,
        errors,
        handleSubmit,
        handleChange
    }
}
